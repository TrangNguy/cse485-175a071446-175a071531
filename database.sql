use dp_myweb;
drop table user;
create table user(
    idUser int(5) unsigned not null primary key AUTO_INCREMENT,
    userName varchar(30) not null,
    userEmail varchar(30) not null,
    userPassword varchar(12) not null,
    userLevel varchar(50) not null,
    dateRegistered date not null,
    userStatus boolean not null
) 