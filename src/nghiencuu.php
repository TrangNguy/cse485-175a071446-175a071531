<html>

<head>
    @@include("partials/head.html", {
    "pageTitle": "Nguyen Tat Thanh University"
    })
</head>

<body>
    <!-- Header
        ================================================== -->
    @@include("partials/header.html")

    <!-- Main
        ================================================== -->
    <main role="main">
        <section>
            <div class="container">
                <div class="row">
                    <div class="sidebar col-md-3 mt-4 p-0 ">
                        <div class="title p-4">
                            <p class="h6">ĐẠI HỌC NGUYỄN TẤT THÀNH</p>
                            <p class="para-first font-weight-bold">"Đoàn kết-Hội nhập-Năng Động-Trí tuệ-Trách nhiệm</p>
                        </div>
                        <div class="description p-4">
                            <img src="assets/imgage/article_sepecial/nc_hinh 1.jpg" alt=""
                                class="img-fluid pb-3">
                            <p>Tính đến nay, Trường đã thực hiện trên 550 đề tài nghiên cứu khoa học các cấp. Trong đó, đội ngũ giảng viên, nghiên cứu viên và sinh viên đã triển khai được:</p>
                            
                            <ul class="font-weight-bold">
                                <li>25 đề tài cấp Nhà nước </li>
                                <li>32 đề tài cấp Bộ;</li>
                                <li>22 đề tài cấp Sở;</li>
                                <li>04 dự án Hợp tác quốc tế</li>
                                <li>516 đề tài cấp Trường</li>
                                <li>712 đề tài sinh viên nghiên cứu khoa học</li>
                                <li>855 bài báo trong và ngoài nước có ISSN</li>
                                <li>671 bài báo trong hệ thống ISI/SCOPUS</li>
                            </ul>
                           <p>Thông qua nhiều đề tài nghiên cứu nhà Trường đã chuyển giao các mô hình, thiết bị, sản phẩm và quy trình công nghệ có đóng góp mới về mặt lý luận khoa học; có giá trị ứng dụng cao.</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <!-- content -->
                        <div class="content pl-3">
                            <img src="assets/imgage/article_sepecial/nc_hinh 2.jpg" alt="" class="img-fluid"
                                width="100%">
                            <article class="article_1 p-4 mb-3">
                                <h2 class="h4">Nghiên cứu</h2>
                                <p>NTTU - Trường ĐH Nguyễn Tất Thành là Trường đào tạo đa ngành, đa lĩnh vực với chất lượng và trình độ cao; kết hợp chặt chẽ giữa đào tạo, bồi dưỡng nguồn nhân lực chất lượng cao với nghiên cứu khoa học và chuyển giao công nghệ; là trung tâm nghiên cứu khoa học và kỹ thuật hiện đại; các nhiệm vụ nghiên cứu khoa học và phát triển công nghệ của Nhà Trường luôn bám sát hướng nghiên cứu phục vụ sự phát triển kinh tế xã hội của Tp. Hồ Chí Minh và đất nước, từng bước tiếp cận chuẩn quốc tế; với mục tiêu xây dựng Nhà trường trở thành trường đại học định hướng nghiên cứu ứng dụng..</p>
                                <a href="#" class="font-weight-bold ">
                                    <i class="fa fa-chevron-circle-right px-2   "></i>http://khcn.ntt.edu.vn/</a>
                            </article>

                            <!-- bai viet -->
                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/hinh 3.1-.jpg" alt=""
                                                    class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Hoạt động khoa học công nghệ</a> </h5>
                                                Nghiên cứu khoa học và phát triển công nghệ trong các lĩnh vực thuộc ngành nghề đào tạo của Trường. Tổ chức Hội thảo, Hội nghị khoa học công nghệ nhằm tăng cường mối quan hệ giữa các nhà khoa học, chia sẻ thông tin về các kết quả nghiên cứu Sản xuất, kinh doanh sản phẩm trên cơ sở các kết quả nghiên cứu khoa học và phát triển công nghệ. Tư vấn, đào tạo bồi dưỡng chuyên môn khác trong các lĩnh vực nghiên cứu nêu trên. Hợp tác với các tổ chức, cá nhân trong và ngoài nước để thực hiện nhiệm vụ của Trường.

                                            </div>
                                        </div> <!-- row -->
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/hinh 3.2.jpg"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Viện - Trung tâm nghiên cứu</a></h5>
                                                NTTU - Khối Kinh tế - Quản trị bao gồm khoa Quản trị kinh doanh, Luật kinh tế và Tài chính – Kế toán. Tận dụng thế mạnh là trường trong doanh nghiệp, là thành viên của Tập đoàn Dệt May Việt Nam, thành viên khối viện – trường Bộ Công thương, việc đào tạo tại các khoa trong khối kinh tế - quản trị luôn gắn liền với đơn vị sử dụng lao động, giúp sinh viên sớm có kinh nghiệm thực tiễn ngay trong quá trình học.
                                                <ul class="list-unstyled pt-2">
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Viện Kỹ thuật Công nghệ cao NTT (NIH)</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Viện Khoa học và công nghệ Industry 4.0</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Viện Khoa học môi trường (IES)</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Viện Nghiên cứu và Đào tạo Văn hóa - Nghệ thuật - Truyền thông</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Viện E-Leaning</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Viện Đào tạo Việt - Hàn</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Viện Nghiên cứu và Đào tạo Văn hóa - Nghệ thuật - Truyền thông</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Trung tâm Nghiên cứu và Đánh giá Giáo dục đại học</a>
                                                        </li>
                                                    </ul>
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/kythuat_congnghe.png"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Công bố khoa học</a></h5>
                                                Trong những năm qua, đội ngũ giảng viên và nghiên cứu viên của Nhà trường đã có nhiều công bố chung trên các tạp chí và hội nghị uy tín trong và ngoài nước từ các liên kết nghiên cứu khoa học với các Viện, trung tâm nghiên cứu uy tín trong nước và quốc tế. Thống kê từ 2011 đến nay, số lượng bài báo khoa học đăng trong tạp chí quốc tế có uy tín trên 250 bài, trên 400 bài bài báo đăng trong tạp chí trong nước, hội nghị quốc tế và trong nước; trong đó có rất nhiều công bố khoa học của cán bộ giảng viên trong trường đã được đăng trên tạp chí thuộc SCOPUS/ISI của các nhà xuất bản hàng đầu của thế giới ScienceDirect, Spinger, Willy….                                                <ul class="list-unstyled pt-2">
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Viet Nam Journal of Computer Science</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Southeast Asian Journal of Science</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>East-West Journal of Mathematics</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Tuyển tập Kỷ yếu Khoa học và Đào tạo NTT</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Bản tin Đánh giá Giáo dục Đại học & Bản tin Giáo dục Quốc tế</a>
                                                        </li>
                                                    </ul>
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>

                        </div> <!-- content -->
                    </div> <!-- col-md-9 -->

                </div>
            </div>
        </section>


    </main>
    <!-- Footer
                ================================================== -->
    @@include("partials/footer.html")

    <!-- JAVASCRIPT
                    ================================================== -->
    @@include("partials/scripts.html")
</body>

</html>