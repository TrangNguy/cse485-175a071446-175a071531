<html>

<head>
<?php
        include("partials/head.html")
        ?>
</head>

<body>
    <!-- Header
        ================================================== -->
        <?php
        include("partials/header.html")
        ?>

    <!-- Main
        ================================================== -->
    <main role="main">
        <section>
        <div class="container">
        <div class="row">
            <div class="sidebar col-md-3 mt-4 p-0 ">
                <div class="title p-4">
                    <p class="h6">ĐẠI HỌC NGUYỄN TẤT THÀNH</p>
                    <p class="para-first font-weight-bold">"Đoàn kết-Hội nhập-Năng Động-Trí tuệ-Trách nhiệm</p>
                </div>
                <div class="description p-4">
                    <img src="assets/imgage/article_sepecial/admission.png" alt="" class="img-fluid pb-3">
                    <p>QUY MÔ ĐÀO TẠO</p>
                    <ul class="font-weight-bold">
                        <li>Hơn 20.000 sinh viên</li>
                    </ul>
                    <p>ĐỘI NGŨ GIẢNG VIÊN</p>
                    <ul class="font-weight-bold">
                        <li>924 giảng viên</li>
                        <li>90% có bằng TS, ThS </li>
                    </ul>
                    <p>CƠ SỞ VẬT CHẤT</p>
                    <ul class="font-weight-bold">
                        <li>8 cơ sở đào tạo </li>
                        <li>100.000 m² sàn xây dựng</li>
                        <li>3.000 máy tính</li>
                        <li>100.000 bản sách trong nước và quốc tế</li>
                        <li>Thư viện đạt chuẩn Quốc gia</li>
                    </ul>
                    <p>CHẤT LƯỢNG</p>
                    <ul class="font-weight-bold">
                        <li>Đạt chuẩn kiểm định chất lượng của Bộ GD&ĐT</li>
                        <li>Đạt chuẩn QS-Stars 3 sao (Anh Quốc)</li>
                    </ul>
                    <p>KHOA</p>
                    <ul class="font-weight-bold">
                        <li><a href="#">Y</a></li>
                        <li><a href="#">Dược</a></li>
                        <li><a href="#">Điều dưỡng</a></li>
                        <li><a href="#">Quản trị Kinh doanh</a></li>
                        <li><a href="#">Luật</a></li>
                        <li><a href="#">Tài chính-Kế toàn</a></li>
                        <li><a href="#">Cơ khí-Điện-Điện tử-Ô tô</a></li>
                        <li><a href="#">Kỹ thuật thực phẩm và Môi trường</a></li>
                        <li><a href="#">Công nghệ Sinh học</a></li>
                        <li><a href="#">Công nghệ Thông tin</a></li>
                        <li><a href="#">Âm nhạc</a></li>
                        <li><a href="#">Kiến trúc-Xây dựng-Mỹ thuật ứng dụng</a></li>
                        <li><a href="#">Ngoại ngữ</a></li>
                        <li><a href="#">Du lịch và Việt Nam học</a></li>
                        <li><a href="#">Giáo dục Quốc phòng-An ninh và Giáo dục Thể chất</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <!-- content -->
                <div class="content pl-3">
                    <img src="assets/imgage/article_sepecial/banner_enquiry.jpg" alt="" class="img-fluid"
                        width="100%">
                    <article class="article_1 p-4 mb-3">
                        <h2 class="h4">Tuyển sinh</h2>
                        <p>Đây là chuyên mục cung cấp một cách nhanh chóng, chính xác toàn cảnh thông tin về: • Tuyển
                            sinh của Trường ĐH Nguyễn Tất Thành qua các năm • Thông tin tuyển sinh mới nhất của Bộ GD&ĐT
                            • Chính sách học bổng tại Trường • Thông tin hướng nghiệp • Tư vấn Để biết thêm thông tin
                            chi tiết, vui lòng truy cập tại địa chỉ: http://tuyensinh.ntt.edu.vn/</p>
                        <a href="#" class="font-weight-bold ">
                            <i class="fa fa-chevron-circle-right px-2   "></i>http://tuyensinh.ntt.edu.vn/</a>
                    </article>

                    <!-- bai viet -->
                    <article>
                        <div class="media py-4 px-0">
                            <div class="media-body pl-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="assets/imgage/article_sepecial/tuyensinh.png" alt=""
                                            class="img-fulid">
                                    </div>
                                    <div class="col-md-8">
                                        <h5><a href="#">Các bậc đào tạo</a> </h5>
                                        NTTU - ĐH Nguyễn Tất Thành là trường đào tạo đa ngành, đa bậc học từ trung cấp,
                                        cao đẳng, đại học cho tới sau đại học. Với mục tiêu hướng đến trường đại học ứng
                                        dụng thực hành, Nhà trường đã đẩy mạnh đầu tư cơ sở vật chất, nâng cao chất
                                        lượng đội ngũ giảng viên và chương trình đào tạo.
                                        <ul class="list-unstyled pt-2">
                                            <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Bậc sau đại học</a>
                                            </li>
                                            <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Bậc đại học</a>
                                            </li>
                                            <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Bậc cao đẳng</a>
                                            </li>
                                            <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Liên thông</a></li>
                                            <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Đào tạo quốc tế</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div> <!-- row -->
                            </div>
                        </div>
                    </article>

                    <article>
                        <div class="media py-4 px-0">
                            <div class="media-body pl-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="assets/imgage/article_sepecial/Admission_Procedure.jpg" alt=""
                                            class="img-fulid">
                                    </div>
                                    <div class="col-md-8">
                                        <h5><a href="#">Thông tin tuyển sinh</a></h5>
                                        NTTU - Đây là bức tranh toàn cảnh về thông tin tuyển sinh Đại học của Trường ĐH
                                        Nguyễn Tất Thành. Chuyên mục này luôn cập nhật các thông tin về quy chế tuyển
                                        sinh, phương thức xét tuyển của Trường ĐH Nguyễn Tất Thành và Bộ Giáo dục & Đào
                                        tạo một cách nhanh chóng và chính xác nhất.

                                    </div>
                                </div> <!-- row -->

                            </div>
                        </div>
                    </article>

                    <article>
                        <div class="media py-4 px-0">
                            <div class="media-body pl-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="assets/imgage/article_sepecial/ScholarshipApplication-720x350.jpg"
                                            alt="" class="img-fulid">
                                    </div>
                                    <div class="col-md-8">
                                        <h5><a href="#">Chính sách học bổng</a></h5>
                                        NTTU – Nhằm động viên, khích lệ tinh thần học sinh – sinh viên vượt qua khó khăn
                                        để tiếp tục con đường học tập, hàng năm nhà trường đã trích ngân sách hơn 20 tỷ
                                        đồng hỗ trợ học sinh – sinh viên đang theo học tại trường.

                                    </div>
                                </div> <!-- row -->

                            </div>
                        </div>
                    </article>

                    <article>
                        <div class="media py-4 px-0">
                            <div class="media-body pl-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="assets/imgage/article_sepecial/tuyensinh_1.png" alt=""
                                            class="img-fulid">
                                    </div>
                                    <div class="col-md-8">
                                        <h5><a href="#">Hướng nghiệp</a> </h5>
                                        NTTU - Nhằm hỗ trợ cho các bạn thí sinh có thể chọn lựa nghề nghiệp phù hợp nhất
                                        với khả năng và nguyện vọng của bản thân. Trường ĐH Nguyễn Tất Thành tự hào là
                                        ngôi trường đào tạo với đa ngành và đa bậc học, ở đây các bạn thí sinh sẽ có
                                        nhiều sự lựa chọn tốt nhất cho nghề nghiệp ổn định trong tương lai của mình.
                                        <ul class="list-unstyled pt-2">
                                            <li> <a href="#"> <i class="fa fa-chevron-right"></i></i>Cẩm nang hướng
                                                    nghiệp</a></li>
                                            <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Tư vấn hướng
                                                    nghiệp</a> </li>
                                        </ul>
                                    </div>
                                </div> <!-- row -->

                            </div>
                        </div>
                    </article>
                    <article>
                        <div class="media py-4 px-0">
                            <div class="media-body pl-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="assets/imgage/article_sepecial/NTTU_dktructuyen.gif" alt=""
                                            class="img-fulid">
                                    </div>
                                    <div class="col-md-8">
                                        <h5><a href="#"> Xét tuyển trực tuyến</a></h5>
                                        NTTU - Tránh tình trạng thí sinh chờ đợi, chen chúc, cũng như vượt đường xa đến
                                        tận trường để nộp hồ sơ, nhiều năm qua trường ĐH Nguyễn Tất Thành đã cho phép
                                        thí sinh được nộp hồ sơ trực tuyến với hình thức đơn giản và dễ dàng. Thí sinh
                                        tham gia xét tuyển có thể đăng ký xét tuyển trực tuyến tại đây

                                    </div>
                                </div> <!-- row -->

                            </div>
                        </div>
                    </article>

                    <article>
                        <div class="media py-4 px-0">
                            <div class="media-body px-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="assets/imgage/article_sepecial/phat-trien-ben-vung.jpg" alt=""
                                            class="img-fulid">
                                    </div>
                                    <div class="col-md-8">

                                        <h5><a href="#"> Phát triển bền vững</a></h5>
                                        Năm 1987, trong Báo cáo “Tương lai chung của chúng ta”, Ủy ban Thế giới
                                        về Môi
                                        trường và Phát triển (WCED-World Commission on Environment and
                                        Development) của
                                        Liên hợp quốc, thì "phát triển bền vững" được định nghĩa là “Sự phát
                                        triển đáp
                                        ứng được nhu cầu của hiện tại mà không làm tổn thương khả năng cho việc
                                        đáp ứng
                                        nhu cầu của các thế hệ tương lai”. Trường Đại học Nguyễn Tất Thành hiểu,
                                        nắm rõ
                                        và thực hiện theo tinh thần này thông qua nhiều hoạt động, chương trình
                                        thực tế
                                        để biến thông điệp thành thói quen nhằm giúp sinh viên thực sự sống và
                                        học tập
                                        có trách nhiệm với môi trường và xã hội xung quanh mình. Gieo mầm xanh,
                                        gặt sự
                                        sống

                                    </div>
                                </div> <!-- row -->

                            </div>
                        </div>
                    </article>
                </div>
            </div>   <!-- col-md-9 -->
            
        </div><!-- row -->
    </div> <!-- container -->
        </section>


    </main>
   <!-- Footer
        ================================================== -->
        <?php
        include("partials/scripts.html")
        ?>

    <!-- JAVASCRIPT
            ================================================== -->
            <?php
        include("partials/scripts.html")
        ?>
</body>

</html>