<html>

<head>
<?php
        include("partials/head.html")
        ?>
</head>

<body>
    <!-- Header
        ================================================== -->
        <?php
        include("partials/header.html")
        ?>

    <!-- Main
        ================================================== -->
    <main role="main">
        <section>
            <div class="container">
                <div class="row">
                    <div class="sidebar col-md-3 mt-4 p-0 ">
                        <div class="title p-4">
                            <p class="h6">ĐẠI HỌC NGUYỄN TẤT THÀNH</p>
                            <p class="para-first font-weight-bold">"Đoàn kết-Hội nhập-Năng Động-Trí tuệ-Trách nhiệm</p>
                        </div>
                        <div class="description p-4">
                            <img src="assets/imgage/article_sepecial/htdn.png" alt=""
                                class="img-fluid pb-3">
                            <ul class="font-weight-bold">
                                <li>Với CLB Doanh nghiệp có 650 thành viên thường xuyên hoạt động.</li>
                                <li>05 Câu lạc bộ doanh nghiệp thuộc các khối ngành gồm: Khối Kinh tế, Khối Khoa học kỹ thuật, khối Sức khoẻ.</li>
                                <li>Giới thiệu việc làm cho 6.415 sinh viên hàng năm.</li>
                                <li>Ban Liên lạc Cựu sinh viên với hơn 1000 cựu sinh viên theo các nhóm ngành nghề khác nhau.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <!-- content -->
                        <div class="content pl-3">
                            <img src="assets/imgage/article_sepecial/cate_HTDN.jpg" alt="" class="img-fluid"
                                width="100%">
                            <article class="article_1 p-4 mb-3">
                                <h2 class="h4">Hợp tác doanh nghiệp</h2>
                                <p>NTTU - Mối quan hệ hợp tác giữa nhà trường và doanh nghiệp là điều có ý nghĩa rất quan trọng. Sự hiệp lực giữa trường đại học với nhà nước và các doanh nghiệp được coi là động lực cốt yếu của những xã hội và những nền kinh tế dựa trên tri thức.</p>
                                <a href="#" class="font-weight-bold ">
                                    <i class="fa fa-chevron-circle-right px-2   "></i>http://vieclam-thuctap.ntt.edu.vn</a>
                            </article>

                            <!-- bai viet -->
                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/[09-03-2017_15.08.09]ngay_hoi_viec_lam.jpg" alt=""
                                                    class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Giới thiệu việc làm</a> </h5>
                                                Với phương Châm thực học – Thực hành – Thực danh và Thực nghiệp nhà trường luôn xem trọng công tác tư vấn giới thiệu việc làm cho sinh viên tốt nghiệp thông qua các ngày hội việc làm, Kỹ năng chinh phục nhà tuyển dụng, Bình quân mỗi năm hơn 1500 vị trí việc làm được giới thiệu cho sinh viên. Công tác này hàng năm giúp tăng tỉ lệ sinh viên trường đại học Nguyễn Tất Thành đạt 94,5% sau tốt nghiệp


                                            </div>
                                        </div> <!-- row -->
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/intership.jpg"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Giới thiệu thực tập</a></h5>
                                                Triết lý đào tạo của nhà trường đảm bảo lợi ích của người học, người dạy nhà trường và xã hội thong qua công tác hỗ trợ sinh viên 100% có chỗ thực tập trong quá trình theo học tại trường. Hàng năm hơn 1000 sinh viên được bộ phận Quan hệ doanh nghiệp – Phòng Công tác Sinh viên giới thiệu đến cộng đồng doanh nghiệp giúp sinh viên rút ngắn khoảng cách giữa lý thuyết và thực hành

                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/clb_doanhnghiep.png"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Câu lạc bộ Doanh nghiệp</a></h5>
                                                Câu lạc bộ doanh nghiệp trường Đại học Nguyễn Tất Thành được thành lập từ năm 2008 với hơn 650 doanh nghiệp thành viên đa dạng ngành nghề phù hợp với khối ngành đào tạo của nhà trường thường xuyên hỗ trợ góp ý chương trình đào tạo Đại học Nguyễn Tất Thành
           
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>
                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/khoahoccoban_thumb.png"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Ban liên lạc Cựu sinh viên</a></h5>
                                                Ban lien lạc cựu sinh viên được hình thành trên nền tảng ý nghĩa thể hiện sự quan tâm của nhà trường dành cho sinh viên sau khi theo học tại trường đồng thời cùng các sinh viên đã thành đạt tiếp tục giúp đỡ các sinh viên đang theo học thong qua các hoạt động quay về trường tuyển dụng các sinh viên khoá sau, hỗ trợ tiếp nhận thực tập sinh viên mới.
           
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/hoptac.png"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Liên kết - Hợp tác</a></h5>
                                                Công tác đào tạo của nhà trường luôn có sự đồng hành gắn bó của công đồng doanh nghiệp thong qua sự hợp tác toàn diện giữa nhà trường với các doanh nghiệp lớn như: Cty Cổ phần Sữa Việt Nam Vinamilk, Vietnam Airlines, Sam Sung, Ngân hàng Sacombank… trong việc cung cấp nguồn nhân lực và hợp tác nghiên cứu khoa học…
           
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>
                        </div> <!-- content -->
                    </div> <!-- col-md-9 -->

                </div>
            </div>
        </section>


    </main>
    <!-- Footer
                ================================================== -->
                <?php
        include("partials/footer.html")
        ?>

    <!-- JAVASCRIPT
                    ================================================== -->
                    <?php
        include("partials/scripts.html")
        ?>
</body>

</html>