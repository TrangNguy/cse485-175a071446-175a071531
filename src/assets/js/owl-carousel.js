$('.owl-carousel-1').owlCarousel({
    loop:true,
    nav:true,
    navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    autoplay:true,
    autoplayTimeout:5000,
    responsive:{
      0:{
          items:1,
          center: true
      },
      576:{
          items:1,
          center: true
      },
      768:{
          items:1,
          center: true
      },
      992:{
          items:1,
          center: true
      },
      1200:{ 
          items:1,
          center: true
        }
    }
});

$('.owl-carousel-2').owlCarousel({
    loop:true,
    nav:true,
    navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    responsive:{
        0:{
            items:1,
            center: true
        },
        600:{
            items:3,
            center: true
        },
        1200:{
            items:3,
            center: true
        }
    }
})

