<html>

<head>
<?php
        include("partials/head.html")
        ?>
</head>

<body>
    <!-- Header
        ================================================== -->
        <?php
        include("partials/header.html")
        ?>

    <!-- Main
        ================================================== -->
    <main role="main">
        <section>
            <div class="container">
                <div class="row">
                    <div class="sidebar col-md-3 mt-4 p-0 ">
                        <div class="title p-4">
                            <p class="h6">ĐẠI HỌC NGUYỄN TẤT THÀNH</p>
                            <p class="para-first font-weight-bold">"Đoàn kết-Hội nhập-Năng Động-Trí tuệ-Trách nhiệm</p>
                        </div>
                        <div class="description p-4">
                            <img src="assets/imgage/phong_Bac_Ho.png" alt="" class="img-fluid pb-3">
                            <p>QUY MÔ ĐÀO TẠO</p>
                            <ul class="font-weight-bold">
                                <li>Hơn 20.000 sinh viên</li>
                            </ul>
                            <p>ĐỘI NGŨ GIẢNG VIÊN</p>
                            <ul class="font-weight-bold">
                                <li>924 giảng viên</li>
                                <li>90% có bằng TS, ThS </li>
                            </ul>
                            <p>CƠ SỞ VẬT CHẤT</p>
                            <ul class="font-weight-bold">
                                <li>8 cơ sở đào tạo </li>
                                <li>100.000 m² sàn xây dựng</li>
                                <li>3.000 máy tính</li>
                                <li>100.000 bản sách trong nước và quốc tế</li>
                                <li>Thư viện đạt chuẩn Quốc gia</li>
                            </ul>
                            <p>CHẤT LƯỢNG</p>
                            <ul class="font-weight-bold">
                                <li>Đạt chuẩn kiểm định chất lượng của Bộ GD&ĐT</li>
                                <li>Đạt chuẩn QS-Stars 3 sao (Anh Quốc)</li>
                            </ul>
                            <p>KHOA</p>
                            <ul class="font-weight-bold">
                                <li><a href="#">Y</a></li>
                                <li><a href="#">Dược</a></li>
                                <li><a href="#">Điều dưỡng</a></li>
                                <li><a href="#">Quản trị Kinh doanh</a></li>
                                <li><a href="#">Luật</a></li>
                                <li><a href="#">Tài chính-Kế toàn</a></li>
                                <li><a href="#">Cơ khí-Điện-Điện tử-Ô tô</a></li>
                                <li><a href="#">Kỹ thuật thực phẩm và Môi trường</a></li>
                                <li><a href="#">Công nghệ Sinh học</a></li>
                                <li><a href="#">Công nghệ Thông tin</a></li>
                                <li><a href="#">Âm nhạc</a></li>
                                <li><a href="#">Kiến trúc-Xây dựng-Mỹ thuật ứng dụng</a></li>
                                <li><a href="#">Ngoại ngữ</a></li>
                                <li><a href="#">Du lịch và Việt Nam học</a></li>
                                <li><a href="#">Giáo dục Quốc phòng-An ninh và Giáo dục Thể chất</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="content pl-3">
                            <img src="assets/imgage/nttu.png" alt="" class="img-fluid" width="100%">
                            <article class="article_1 p-4 mb-3">
                                <h2 class="h4">Giới thiệu</h2>
                                <p>NTTU : Trường đại học Nguyễn Tất Thành được đặt tại Quận 4. Từ Trường đến trung tâm
                                    thành phố Hồ Chí Minh chỉ mất 5 phút đi bằng ô-tô. Trường nằm cạnh bờ sông Sài Gòn
                                    lộng gió. Từ trên sân thượng tòa nhà, phóng tầm mắt ra xa, chúng ta có thể thấy bao
                                    quát toàn thành phố. Phía trước tòa nhà là dòng sông Sài Gòn dịu dàng uốn quanh, với
                                    nhiều tàu thuyền qua lại.</p>
                                <a href="#" class="font-weight-bold ">
                                    <i class="fa fa-chevron-circle-right px-2   "></i>http://ntt.edu.vn</a>
                            </article>

                            <!-- bai viet -->
                            <article>
                                <div class="media py-4 my-3 px-0">
                                    <div class="media-body pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/academics_thumb.png"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Giới thiệu chung</a> </h5>
                                                Ra đời từ chủ trương xã hội hóa giáo dục, Trường Đại học Nguyễn Tất
                                                Thành đã
                                                không ngừng đầu tư, xây dựng và phát triển để trở thành một trường đại
                                                học
                                                đẳng cấp quốc gia và hội nhập quốc tế. Sau 20 năm, đến này trường đã có
                                                những bước tiến vượt trội trong công tác đào tạo với 15 khoa, 44 chương
                                                trình đào tạo bậc đại học thuộc 05 khối ngành: Sức khỏe, Kinh tế, Xã hội
                                                –
                                                Nhân văn, Kỹ thuật – Công nghệ, Nghệ thuật – Mỹ thuật.
                                                <ul class="list-unstyled mb-0 pt-2 mb-0">
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Thư ngỏ Hiệu
                                                            trưởng</a> </li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Văn hóa ĐH
                                                            Nguyễn Tất Thành</a> </li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Bài ca Đại
                                                            học Nguyễn Tất Thành</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Tầm nhìn -
                                                            Sứ mạng</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Các thành
                                                            tích đạt được</a></li>
                                                </ul>
                                            </div>
                                        </div> <!-- row -->
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media my-4 py-4 px-0">
                                    <div class="media-body my-3 my-3 pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/vanhoa_NTT-thumb.png"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Toàn cảnh ĐH Nguyễn Tất Thành</a></h5>
                                                Trải qua quá trình 20 năm hình thành và phát triển, đến nay, Trường Đại
                                                học
                                                Nguyễn Tất Thành đã khẳng định được uy tín và danh hiệu của mình trên
                                                bản đồ
                                                giáo dục Việt Nam.
                                                <ul class="list-unstyled mb-0 pt-2">
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Những cột
                                                            mốc phát triển</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Một số
                                                            thành tích của sinh viên</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Hợp tác quốc
                                                            tế</a> </li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Những con
                                                            số ấn tượng</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Các đề tài
                                                            nghiên cứu khoa học</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Hợp tác
                                                            doanh nghiệp</a></li>
                                                </ul>
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body my-3 pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/vanhoa_NTT-thumb.png"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#"> Hệ thống Cơ sở vật chất</a></h5>
                                                Theo thang điểm đánh giá của Tổ chức QS - Stars Anh quốc thì tiêu chuẩn
                                                cơ sở
                                                vật chất của Trường ĐH Nguyễn Tất Thành được tổ chức này đánh giá với số
                                                điểm
                                                tuyệt đối: 5 sao - mức độ cao nhất trong đánh giá gắn sao của QS -
                                                Stars.
                                                <ul class="list-unstyled pt-2">
                                                    <li> <a href="#"> <i class="fa fa-chevron-right"></i></i> Cơ sở An
                                                            Phú Đông - Quận 12</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Cơ sở Nguyễn
                                                            Hữu Thọ - Quận 7</a> </li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Cơ sở 300A
                                                            - Quận 4</a></li>
                                                </ul>
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body my-3 pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/khoahoccoban_thumb.png"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Thông tin 3 công khai</a> </h5>
                                                Theo quy định của Bộ GD&ĐT tại Thông tư 09/2009/TT-BGDĐT ngày 07/05/2009
                                                các cơ
                                                sở giáo dục đại học phải có trách nhiệm công khai minh bạch các tiêu
                                                chuẩn để
                                                học sinh và phụ huynh có thể lựa chọn môi trường học tập phù hợp cho
                                                mình; đồng
                                                thời cũng tạo điều kiện cho xã hội có theo dõi, giám sát quá trình dạy
                                                và học
                                                của các cơ sở.
                                                <ul class="list-unstyled pt-2">
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>3 công khai
                                                            NH 2018 - 2019</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>3 công khai
                                                            NH 2016-2017</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>3 công khai
                                                            NH 2014-2015</a> </li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>3 công khai
                                                            NH 2012-2013</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>3 công khai
                                                            NH 2017 - 2018</a> </li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> 3 công khai
                                                            NH 2015 - 2016</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>3 công khai
                                                            NH 2013 - 2014</a> </li>
                                                </ul>
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>
                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body my-3 pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/dambaochatluong.png"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#"> Đảm bảo chất lượng</a></h5>
                                                Đáp ứng yêu cầu về tuyển sinh, kiểm định chất lượng giáo dục đại học và
                                                trách
                                                nhiệm giải trình với xã hội trong bối cảnh hội nhập, Trường Đại học
                                                Nguyễn Tất
                                                Thành đã thực hiện tự đánh giá cấp Chương trình đào tạo và cấp Cơ sở đào
                                                tạo
                                                (tức cấp Trường) theo Bộ tiêu chuẩn chất lượng của Bộ giáo dục và Đào
                                                tạo. Các
                                                hoạt động đảm bảo chất lượng trong Nhà trường đều hướng đến việc thực
                                                hiện thành
                                                công sứ mạng, tầm nhìn, mục tiêu giáo dục, và các giá trị cốt lõi của
                                                Trường.

                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body my-3 px-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/phat-trien-ben-vung.jpg"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">

                                                <h5><a href="#"> Phát triển bền vững</a></h5>
                                                Năm 1987, trong Báo cáo “Tương lai chung của chúng ta”, Ủy ban Thế giới
                                                về Môi
                                                trường và Phát triển (WCED-World Commission on Environment and
                                                Development) của
                                                Liên hợp quốc, thì "phát triển bền vững" được định nghĩa là “Sự phát
                                                triển đáp
                                                ứng được nhu cầu của hiện tại mà không làm tổn thương khả năng cho việc
                                                đáp ứng
                                                nhu cầu của các thế hệ tương lai”. Trường Đại học Nguyễn Tất Thành hiểu,
                                                nắm rõ
                                                và thực hiện theo tinh thần này thông qua nhiều hoạt động, chương trình
                                                thực tế
                                                để biến thông điệp thành thói quen nhằm giúp sinh viên thực sự sống và
                                                học tập
                                                có trách nhiệm với môi trường và xã hội xung quanh mình. Gieo mầm xanh,
                                                gặt sự
                                                sống

                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </main>
    <!-- Footer
        ================================================== -->
        <?php
        include("partials/scripts.html")
        ?>

    <!-- JAVASCRIPT
            ================================================== -->
            <?php
        include("partials/scripts.html")
        ?>
</body>

</html>