<nav class="navbar navbar-expand-lg navbar-light ">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebar-collapse"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
</nav>

<div class="menu-admin col-sm-3 col-lg-2 collapse navbar-collapse" id="sidebar-collapse">
    <ul class="navbar-nav ">
        <li class="nav-item  active"><a href="PageAdmin.php" class="nav-link  px-2 d-flex align-items-center "><i
                    class=" mr-2 material-icons icon">
                    dashboard
                </i>Dashboard</a></li>
        <li class="nav-item"><a href="view-user.php" class="nav-link px-2 d-flex align-items-center"><i
                    class=" mr-2 material-icons icon">
                    person
                </i>User Profile</a></li>
        <li class="nav-item"><a href="post.php" class="nav-link px-2 d-flex align-items-center"><i
                    class=" mr-2 material-icons icon">
                    settings
                </i>New post</a></li>
        <li class="nav-item"><a href="#" class="nav-link sideMenuToggler px-2 d-flex align-items-center"><i
                    class="mr-2 material-icons icon">view_list
                </i></a></li>
    </ul>
</div>