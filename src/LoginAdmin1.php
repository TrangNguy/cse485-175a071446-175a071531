<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../node_modules/owl.carousel2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="../node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/theme.css">

</head>

</head>

<body>
<?php
        
if($_SERVER['REQUEST_METHOD']=='POST'){
    require('process-login.php');
}

?>
    <div class="loginAdmin mx-0">
    <div class="container">
        <div class="row py-5 ">
            <form class="loginaccount col-sm-4 offset-sm-4 text-center px-3 " method="post" action="process-login.php">
                <div>
                    <img src="../src/assets/imgage/logoregister_nttu.jpg" class="img-rounded center-block" width="100" />
                </div>

                <h2 class="login pt-4">Account Login</h3>
                    <div class="container">
                                <div class="position-relative form-group">
                                    <i
                                        class="fa fa-user position-absolute d-flex justify-content-left h-100 mx-2 align-items-center"></i>
                                    <input type="email" class="form-control pl-4" id="inputEmail" name="txtEmail"
                                        placeholder="Enter Email">
                                </div>

                                <div class="position-relative form-group ">
                                    <i
                                        class="fa fa-lock position-absolute d-flex justify-content-left h-100 mx-2 align-items-center"></i>
                                    <input type="password" class="form-control pl-4" id="inputPassword" name="txtPassword"
                                        placeholder="Password">
                                </div>
                            
                           
                                <button type="submit" class="btn btn-primary btn-block mt-4">Login</button>
                           
                      
                    </div>
            </form>
        </div> <!--row-->
        </div>  <!--container-->
    </div>

    <!-- JAVASCRIPT
            ================================================== -->
          <!-- Libs JS -->
<script src="../node_modules/jquery/dist/jquery.min.js"></script>
<script src="../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

<!-- # -->

    

</body>

</html>


