<html>

<head>
        
        <?php
        include("partials/head.html")
        ?>
</head>

<body>
    <!-- Header
        ================================================== -->
        <?php
        include("partials/header.html")
        ?>
    <!-- @@include("partials/header.html") -->
    <!-- Main
        ================================================== -->
        <?php
           require('connect-DB.php');
          

        ?>
    <main role="main">
        <div class="container">
            <section>
                <!-- carousel -->
                <div class="owl-carousel owl-carousel-1 owl-theme">
                    <div class="item ">
                        <img src="assets/imgage/slide-show/slide-show-2-edit.png" alt="">
                        <div class="caption">
                            <h1 class="h3 text-white  w-100 text-left ">Chính sách học bổng Trường đại học Nguyễn Tất
                                Thành 2019</h1>
                        </div>
                    </div>
                    <div class="item ">
                        <img src="assets/imgage/slide-show/slide-show-3-edit.png" alt="">
                        <div class="caption">
                            <h1 class="h3 text-white  w-100 text-left ">Tuyển sinh liên thông 2019</h1>
                        </div>
                    </div>

                    <div class="item ">
                        <img src="assets/imgage/slide-show/slide-show-8-edit.png" alt="">
                        <div class="caption">
                            <h1 class="h3 text-white  w-100 text-left">Tuyển sinh trình độ Thạc sỹ đợt
                                1 năm 2019 tại ĐH Nguyễn Tất Thành</h1>
                        </div>

                    </div>
                    <div class="item ">
                        <img src="assets/imgage/slide-show/slide-show-5.jpg" alt="">
                        <div class="caption">
                            <h1 class="h3 text-white  w-100 text-left ">Viện đào tạo quốc tế NTT (NIIE)
                                - "Cử nhân quốc tế, Cơ hội toàn cầu"</h1>
                        </div>

                    </div>
                    <div class="item ">
                        <img src="assets/imgage/slide-show/slide-show-6.jpg" alt="">
                        <div class="caption">
                            <h1 class="h3 text-white  w-100 text-left ">Giải đấu Futsal SV Thành phố Hồ
                                Chí Minh 2019
                            </h1>
                        </div>
                    </div>
                    <div class="item ">

                        <img src="assets/imgage/slide-show/slide-show-6.jpg" alt="">
                        <div class="caption">
                            <h1 class="h3 text-white  w-100 text-left ">Giải đấu Futsal SV Thành phố Hồ
                                Chí Minh 2019
                            </h1>
                        </div>

                    </div>
                    <div class="item">
                        <img src="assets/imgage/slide-show/slide-show-9.png" alt="">
                        <div class="caption">
                            <h1 class="h3 text-white  w-100 text-left ">Lịch phát sóng Chương trình
                                Tham vấn Chuyên gia</h1>
                        </div>
                    </div>

                </div> <!-- carousel -->

                <!-- Tin tuc, media -->
                <div class="section-article mt-4 mb-5">
                    <div class="row">
                        <div class="col-md-9">
                            <h2 class="h4 mb-4 position-relative">Tin tức</h2>
                            
                            <div class="row">
                            <?php
                             $sql = "SELECT * FROM posts ORDER BY PostId DESC LIMIT 3";
                             $result = mysqli_query($connect, $sql);
                             
                             if($result){
                                 while($row=mysqli_fetch_assoc($result)){
                                    
                                    ?>
                                    <article class="col-md-4 col-sm-6 article text-justify">
                                    <a href="#"> <img src="upload/<?php echo $row['Image']; ?>" alt=""
                                            class="thumbnail img-fluid w-100"></a>
                                    <div class="mb-2 mt-3 h6 title-article ">
                                        <a href="#">
                                           <?php echo $row['post_title'];?></a>
                                    </div>

                                    <div class="content ">
                                        <p> <?php echo $row['description']; ?></p>
                                    </div>

                                </article>

                                     <?php
                                 }
                             }
                            ?>
                              
                            </div>
                            <div class="link-list-article d-flex justify-content-end">

                                <a href="#" class="font-weight-bold ">
                                    <i class="fa fa-chevron-circle-right px-2"></i>Xem thêm</a>

                            </div>
                        </div>


                        <div class="col-md-3 media-video">
                            <h2 class="h4 mb-4 position-relative">Media</h2>

                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/JViDyojZzhs"
                                    allowfullscreen></iframe>
                            </div>

                            <div class="list-video">
                                <ul class="px-0">
                                    <li class="mt-2"> <i class="fa fa-chevron-right"></i>
                                        <a href="#" class="text-black-50 ml-2">ĐIỂM TIN THÁNG 8</a> </li>
                                    <li class="mt-2"> <i class="fa fa-chevron-right"></i>
                                        <a href="#" class="text-black-50 ml-2">ĐH Nguyễn Tất Thành - 20 năm một dấu ấn
                                            chặng đường</a> </li>
                                    <li class="mt-2"> <i class="fa fa-chevron-right"></i>
                                        <a href="#" class="text-black-50 ml-2">NỎ THẦN - TẬP 8| BẬT BÍ VỀ NỎ THẦN VÀ
                                            NHỮNG CẦU CHUYỆN CÓ THẬT</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="link-list-article d-flex justify-content-end">

                                <a href="#" class="font-weight-bold "> <i
                                        class="fa fa-chevron-circle-right px-2"></i>Xem thêm</a>

                            </div>

                        </div>

                    </div>

                </div>

                <!-- sub-slider -->
                <div class="section-reason color-blue mb-5">
                    <h2 class="h3 title-carousel text-uppercase text-center py-4">Tại sao chọn đại học nguyễn tất thành
                    </h2>
                    <div class="owl-carousel-2 owl-carousel owl-theme pt-5 px-4">
                        <div class="item ">
                            <div class=" text-center text-white">
                                <a href="#"
                                    style="text-decoration: none; color: inherit">Đại học hạnh phúc</a>
                                <p>Đại học Nguyễn Tất Thành là ngôi trường tri thức và hạnh
                                    phúc dành
                                    cho sinh viên với 5 giá trị nổi bật</p>
                            </div>
                        </div>
                        <div class="item ">
                            <div class=" text-center text-white">
                                <a href="#"
                                        style="text-decoration: none; color: inherit">Top 10 thương hiệu nổi
                                        tiếng</a>
                                <p>Trong những năm qua, Trường ĐH Nguyễn Tất Thành đã không
                                    ngừng đổi
                                    mới công tác quản trị đại học, nâng cao chất lượng đào tạo, nghiên cứu khoa
                                    học,
                                    công tác hợp tác quốc tế, công tác sinh viên.</p>
                            </div>
                        </div>
                        <div class="item ">
                            <div class="text-center text-white">
                                <p><a href="#"
                                        style="text-decoration: none; color: inherit">95% sinh viên tốt nghiệp
                                        có việc làm</a></p>
                                <p>Định vị là trường ứng dụng và thực hành hướng tới mục tiêu
                                    đáp ứng
                                    nhu cầu giáo dục đại học đại chúng, trí thức hóa nguồn nhân lực, tạo môi
                                    trường
                                    học tập tích cực và trải nghiệm thực tiễn cho sinh viên.</p>
                            </div>
                        </div>
                        <div class="item ">
                            <div class="text-center text-white">
                                <p><a href="#"
                                        style="text-decoration: none; color: inherit">Đạt chuẩn chất lượng quốc
                                        gia</a></p>
                                <p>Là trường đại học ngoài công lập đầu tiên tại TP.HCM được
                                    kiểm định đạt chất lượng theo bộ tiêu chí quốc gia do Bộ GD&amp;ĐT ban hành.</p>
                            </div>
                        </div>
                        <div class="item ">
                            <div class=" text-center text-white">
                                <p><a href="#"
                                        style="text-decoration: none; color: inherit">Top 10 thương hiệu nổi
                                        tiếng</a></p>
                                <p>Trong những năm qua, Trường ĐH Nguyễn Tất Thành đã không ngừng đổi
                                    mới công tác quản trị đại học, nâng cao chất lượng đào tạo, nghiên cứu khoa
                                    học,
                                    công tác hợp tác quốc tế, công tác sinh viên.</p>
                            </div>

                        </div>
                        <div class="item ">
                            <div class="text-center text-white">
                                <p><a href="#"
                                        style="text-decoration: none; color: inherit">95% sinh viên tốt nghiệp
                                        có việc làm</a></p>
                                <p>Định vị là trường ứng dụng và thực hành hướng tới mục tiêu đáp ứng
                                    nhu cầu giáo dục đại học đại chúng, trí thức hóa nguồn nhân lực, tạo môi
                                    trường học tập tích cực và trải nghiệm thực tiễn cho sinh viên.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="text-center text-white">
                                <p><a href="#"
                                        style="text-decoration: none; color: inherit">95% sinh viên tốt nghiệp
                                        có việc làm</a></p>
                                <p>Định vị là trường ứng dụng và thực hành hướng tới mục tiêu đáp ứng
                                    nhu cầu giáo dục đại học đại chúng, trí thức hóa nguồn nhân lực, tạo môi
                                    trường học tập tích cực và trải nghiệm thực tiễn cho sinh viên.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class=" text-center text-white">
                                <p><a href="#"
                                        style="text-decoration: none; color: inherit">Chuẩn 3 sao QS-Stars (Anh
                                        Quốc)</a></p>
                                <p>Xếp hạng quốc tế 3 sao theo chuẩn QS-Stars, một trong các chuẩn xếp
                                    hạng hàng đầu dành cho các trường đại học trên thế giới.</p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="d-block  text-center text-white">
                                <p><a href="#"
                                        style="text-decoration: none; color: inherit">Đạt chuẩn chất lượng quốc
                                        gia</a></p>
                                <p>Là trường đại học ngoài công lập đầu tiên tại TP.HCM được kiểm định
                                    đạt chất lượng theo bộ tiêu chí quốc gia do Bộ GD&amp;ĐT ban hành.</p>
                            </div>
                        </div>

                    </div> <!-- carousel -->
                </div> <!-- section-reason  -->


                <!-- sub-slider -->
                <hr>
                <!-- Hơp tac quoc te -->
                <div class="section-co-operate mt-4">
                    <div class="row">
                        <div class="col-lg-8 col-md-6">
                            <div class="row">
                                <article class="col-md-6 col-sm-6 mt-0 article text-justify">
                                    <h2 class="h4 mb-4 position-relative">Hợp tác quốc tế</h2>
                                    <a href=""> <img src="assets/imgage/article/article-1.jpg" alt=""
                                            class="thumbnail img-fluid w-100"></a>
                                    <div class="mb-2 mt-3 h6 title-article ">
                                        <a href="#" class="mb-2 mt-3">Sinh viên ĐH Nguyễn Tất Thành hào hứng tham gia
                                            giao
                                            lưu văn hóa cùng sinh viên ĐH Gakushuin (Nhật Bản)</a>
                                    </div>
                                    <div class="content">
                                        <p>NTTU – Sáng ngày 09/09/2019, tại cơ sở An Phú Đông, Trường ĐH Nguyễn Tất
                                            Thành đã
                                            tổ chức chương trình giao lưu văn hóa với sinh viên Trường ĐH Gakushuin
                                            (Nhật
                                            Bản)</p>
                                    </div>
                                    <div class="link-list-article d-flex justify-content-end">

                                        <a href="#" class="font-weight-bold ">
                                            <i class="fa fa-chevron-circle-right px-2"></i>Xem thêm</a>

                                    </div>

                                </article>
                                <article class="col-md-6 col-sm-6 mt-0 article text-justify">
                                    <h2 class="h4 mb-4 position-relative">Môi trường học tập</h2>
                                    <a href=""> <img src="assets/imgage/article/article-2.jpg" alt=""
                                            class="thumbnail img-fluid w-100"></a>
                                    <div class="mb-2 mt-3 h6 title-article ">
                                        <a href="" class="mb-2 mt-3">Mang ánh trăng về các vùng quê nghèo</a>
                                    </div>
                                    <div class="content">
                                        <p>NTTU – Thấu hiểu hoàn cảnh của những trẻ em nghèo trên mọi vùng quê Việt Nam,
                                            với
                                            mong muốn mang đến cho các một tết trung thu ấm áp, đầy ý nghĩa, Đoàn – Hội
                                            sinh
                                            viên Trường ĐH Nguyễn Tất Thành đã tổ chức nhiều hoạt động nhằm mang trung
                                            thu
                                            đến cho các em thiếu nhi</p>

                                    </div>
                                    <div class="link-list-article d-flex justify-content-end">

                                        <a href="#" class="font-weight-bold ">
                                            <i class="fa fa-chevron-circle-right px-2"></i>Xem thêm</a>

                                    </div>
                                </article>
                            </div>
                        </div>

                        <!-- list-event -->
                        <div class="col-lg-4 col-md-6">
                            <h2 class="h4 mb-4 position-relative">Sự kiện nổi bật</h2>
                            <div class="list-event d-flex  ">
                                <div
                                    class="date color-blue text-white px-4 mr-3 d-flex align-items-center justify-content-center">
                                    <span class="text-center">
                                        Sep<br><b>14</b>
                                    </span>

                                </div>
                                <div class="content mb-2">
                                    <a href="#" class="font-weight-bold">Chuyên đề tốt nghiệp lớp Việt Nam học - Khoa Du
                                        lịch và Việt Nam học</a>
                                    <ul class="breadcrumb mb-0 bg-white px-0">
                                        <li class="breadcrumb-item time">
                                            <i class="fa fa-clock-o mr-2" aria-hidden="true"></i>07:00
                                        </li>
                                        <li class="breadcrumb-item col-xs-auto text-black-50">Sân bóng - cơ sở quận 7,
                                            458/3F Nguyễn Hữu Thọ, phường Tân Hưng, quận 7</li>
                                    </ul>
                                </div>


                            </div>
                            <!-- event1 -->
                            <div class="list-event d-flex">
                                <div
                                    class="date color-blue text-white px-4 mr-3 d-flex align-items-center justify-content-center">
                                    <span class="text-center">
                                        Sep<br><b>14</b>
                                    </span>

                                </div>
                                <div class="content mb-2">
                                    <a href="#" class="font-weight-bold">Workshop, Chủ đề: "Youtube đã thay đổi cuộc
                                        sống của tôi như thế nào''</a>
                                    <ul class="breadcrumb mb-0 bg-white">
                                        <li class="breadcrumb-item col-xs-auto time px-0">
                                            <i class="fa fa-clock-o mr-2" aria-hidden="true"></i> 07:30
                                        </li>
                                        <li class="breadcrumb-item col-xs-auto text-black-50">Hội trường L.HT1, 331
                                            QL1A, phường An
                                            Phú Đông, quận 12</li>
                                    </ul>
                                </div>


                            </div>
                            <!-- event2 -->
                            <div class="list-event d-flex">
                                <div
                                    class="date color-blue text-white px-4 mr-3 d-flex align-items-center justify-content-center">
                                    <span class="text-center">
                                        Sep<br><b>14</b>
                                    </span>

                                </div>
                                <div class="content mb-2">
                                    <a href="#" class="font-weight-bold">Lễ ký kết hợp tác với Tạp chí Dân chủ & Pháp
                                        luật</a>
                                    <ul class="breadcrumb bg-white px-0">
                                        <li class="breadcrumb-item col-xs-auto time">
                                            <i class="fa fa-clock-o mr-2" aria-hidden="true"></i>08:00
                                        </li>
                                        <li class="breadcrumb-item col-xs-auto text-black-50">Phòng họp 1 - Cơ sở An Phú
                                            Đông</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- event3 -->
                            <div class="link-list-article d-flex justify-content-strart">

                                <a href="#" class="font-weight-bold ">
                                    <i class="fa fa-chevron-circle-right px-2"></i>Xem thêm</a>

                            </div>
                        </div>

                    </div>

                </div>

            </section>
        </div>




    </main>

    <!-- Footer
        ================================================== -->
        <?php
        include("partials/footer.html")
        ?>
    <!-- @@include("partials/footer.html") -->

    <!-- JAVASCRIPT
        ================================================== -->
    <!-- @@include("partials/scripts.html") -->
    <?php
        include("partials/scripts.html")
        ?>
    <!-- <script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="node_modules/owl.carousel2/dist/owl.carousel.min.js"></script> -->
<!-- # -->
<!-- <script src="assets/js/owl-carousel.js"></script>
<script src="assets/js/contact.js"></script>
<script src="assets/js/button_header.js"></script> -->

</body>

</html>