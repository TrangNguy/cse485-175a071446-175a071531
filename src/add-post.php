<?php
session_start();
if (!isset($_SESSION['user_level']))
{ 
   header("Location: LoginAdmin1.php");
   exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Header
        ================================================== -->
    <?php
        include("partials/head.html")
        ?>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-icons/3.0.1/iconfont/material-icons.min.css" />
</head>

<body>
    <?php
        include("partials/scripts.html")
        ?>
    <div class=" admin p-0">
        <!-- nav-header -->
        <nav class="navbar navbar-expand-md navbar-admin navbar-light d-flex justify-content-between ">
            <div class="brand my-4">
                <div class="logo">
                    <span class="l l1"></span>
                    <span class="l l2"></span>
                    <span class="l l3"></span>
                    <span class="l l4"></span>
                    <span class="l l5"></span>
                </div> NTT Admin
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebar-collapse"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <a class="btn btn-outline-success " href="process-logout.php">Log out</a>
        </nav>
        <div class="row no-gutters">
            <div class="menu-admin col-sm-3 col-lg-3 navbar-collapse " id="sidebar-collapse">
                <ul class="navbar-nav ">
                    <li class="nav-item  active"><a href="PageAdmin.php"
                            class="nav-link  px-2 d-flex align-items-center "><i class=" mr-2 material-icons icon">
                                dashboard
                            </i>Dashboard</a></li>
                    
                    <li class="nav-item"><a href="post.php" class="nav-link px-2 d-flex align-items-center"><i
                                class=" mr-2 material-icons icon">
                                settings
                            </i>New post</a></li>
                            <li class="nav-item">
                        <a href="list-category.php"
                                class="nav-link sideMenuToggler px-2 d-flex align-items-center">
                                <i class="mr-2 material-icons icon">view_list
                                </i>List category</a></li>
                </ul>
            </div>

            <div class="col-sm-9  col-lg-9 content   ">
               
                <div class="add-post mx-3 ">
                    <div class="title">
                        <h4 class="text-uppercase">Add new post</h4>
                        <hr>
                    </div>
                    <div class="content">

                        <form method="post" action="process-add-post.php" enctype="multipart/form-data">
                            <div class="card p-5 mb-5">
                                <div class="form-group row">
                                    <label class="col-sm-2 font-weight-bold"> Title: </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder="" name="txtTitle" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 font-weight-bold"> Description: </label>
                                    <div class="col-sm-10">
                                        <label for="Description">Writing Description</label>
                                        <textarea class="form-control" id="Description" name="txtDes"
                                            required></textarea>
                                        <script>
                                        CKEDITOR.replace('Description');
                                        </script>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 font-weight-bold"> Content: </label>
                                    <div class="col-sm-10">
                                        <label for="Content">Writing Content</label>
                                        <textarea class="form-control" id="Content" rows="3" name="txtContent"
                                            required></textarea>
                                        <script>
                                        CKEDITOR.replace('Content');
                                        </script>
                                    </div>
                                </div>
                                <!-- get data category -->

                                <div class="form-group row">
                                    <label class="col-sm-2 font-weight-bold"> Category: </label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="category">
                                            <?php 
                                         require('connect-DB.php');
                                          $sql = "select * from category";
                                          $result = mysqli_query($connect, $sql);
                                          $i = 1;
                                          if($result){
                                    
                                            while($row = mysqli_fetch_assoc($result))
                                               {
                                                   echo '<option value="'.$row['ID'].'">'.$row['Name'].'</option>';
                                                   $i++;
                                               }       
                                          }
                                          else {
                                              echo "loi";
                                          }
                                            
                                     ?>
                                        </select>
                                    </div>
                                </div>
                                <?php
                                    
                           ?>
                                <div class="form-group row">
                                    <label class="col-sm-2 font-weight-bold"> Images: </label>
                                    <div class="col-sm-10">
                                        <div class="images-container">
                                            <input type="file" name="txtImg">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row  ">
                                    <div class="col-sm-10 col-sm-offset-2">
                                        <button type="submit" class="btn btn-primary"> Add new post</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- JAVASCRIPT
            ================================================== -->

</body>

</html>