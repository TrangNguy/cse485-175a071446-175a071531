<?php
session_start();

if (!isset($_SESSION['user_level']) )
{ 
   header("Location: LoginAdmin1.php");
   exit();
   
}


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Header
        ================================================== -->
    <?php
        include("partials/head.html")
        ?>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-icons/3.0.1/iconfont/material-icons.min.css" />
</head>

<body>
    <div class=" container admin p-0">
        <!-- nav-header -->
        <nav class="navbar navbar-expand-md navbar-admin navbar-light d-flex justify-content-between ">
        <div class="brand my-4">
                                <div class="logo">
                                    <span class="l l1"></span>
                                    <span class="l l2"></span>
                                    <span class="l l3"></span>
                                    <span class="l l4"></span>
                                    <span class="l l5"></span>
                                </div> NTT Admin
                            </div>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebar-collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

                <a class="btn btn-outline-success " href="process-logout.php">Log out</a>
        </nav>
        <div class="row no-gutters">
            <div class="menu-admin col-sm-3 col-lg-3 navbar-collapse " id="sidebar-collapse">
                <ul class="navbar-nav ">
                    <li class="nav-item  active"><a href="PageAdmin.php"
                            class="nav-link  px-2 d-flex align-items-center "><i class=" mr-2 material-icons icon">
                                dashboard
                            </i>Dashboard</a></li>
                    <li class="nav-item"><a href="view-user.php" class="nav-link px-2 d-flex align-items-center"><i
                                class=" mr-2 material-icons icon">
                                person
                            </i>User Profile</a></li>
                    <li class="nav-item"><a href="post.php" class="nav-link px-2 d-flex align-items-center"><i
                                class=" mr-2 material-icons icon">
                                settings
                            </i>New post</a></li>
                    <li class="nav-item"><a href="#" class="nav-link sideMenuToggler px-2 d-flex align-items-center"><i
                                class="mr-2 material-icons icon">view_list
                            </i></a></li>
                </ul>
            </div>

            <div class="col-sm-9  col-lg-9 content   ">
              
                    <div class="header w-100 d-flex justify-content-end mt-3">
                        <button class="btn btn-outline-success  mx-5" data-toggle="modal" data-target="#addUser">Add
                            user</button>
                        
                        <?php
                                                        if($_SERVER['REQUEST_METHOD']=='POST'){
                                                            require('process-add-user.php');
                                                        }
                                                ?>
                        <div class="modal fade" id="addUser" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalScrollableTitle">Add User</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="mb-6" method="post">

                                            <div class="form-group">
                                                <label for="userName">
                                                    User Name
                                                </label>
                                                <input type="text" class="form-control" id="userName"
                                                    placeholder="User Name" name="txtName" required>
                                            </div>

                                            <!-- Email -->
                                            <div class="form-group">
                                                <label for="email">
                                                    Email Address
                                                </label>
                                                <input type="email" class="form-control" id="email"
                                                    placeholder="name@address.com" required name="txtEmail">
                                            </div>

                                            <!-- Password -->
                                            <div class="form-group mb-5">
                                                <label for="password">
                                                    Password
                                                </label>
                                                <input type="password" class="form-control" id="password" minlength="8"
                                                    maxlength="12" placeholder="Enter password" required
                                                    name="txtPassword">
                                                <span>Between 8 and 12 characters.</span>
                                            </div>
                                            <div class="form-group mb-5">
                                                <label for="level">
                                                    Level
                                                </label>
                                                <select name="Level" id="">
                                                    <option value="1" name="Admin" selected>Admin</option>
                                                    <option value="0" name="Commissioner" selected>Commissioner</option>

                                                </select>

                                            </div>
                                            <div class="form-group mb-5">
                                                <label for="Date">
                                                    Date Registered
                                                </label>
                                                <input type='date' class="form-control" required name="txtDate" />
                                            </div>

                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- header -->

                    <div class="col-sm-9  col-lg-9 content   ">
                    <div class="header w-100 d-flex justify-content-end mt-3">

                        <?php
                        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                            require('process-edit-user.php');
                        }
                        ?>
                        <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalScrollableTitle">Edit User</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="mb-6" method="post">

                                            <div class="form-group">
                                                <label for="userName">
                                                    User Name
                                                </label>
                                                <input type="text" class="form-control" id="userName" placeholder="User Name" name="txtName" required>
                                            </div>

                                            <!-- Email -->
                                            <div class="form-group">
                                                <label for="email">
                                                    Email Address
                                                </label>
                                                <input type="email" class="form-control" id="email" placeholder="name@address.com" required name="txtEmail">
                                            </div>

                                            <!-- Password -->
                                            <div class="form-group mb-5">
                                                <label for="password">
                                                    Password
                                                </label>
                                                <input type="password" class="form-control" id="password" minlength="8" maxlength="12" placeholder="Enter password" required name="txtPassword">
                                                <span>Between 8 and 12 characters.</span>
                                            </div>
                                            <div class="form-group mb-5">
                                                <label for="level">
                                                    Level
                                                </label>
                                                <select name="Level" id="">
                                                    <option value="1" name="Admin" selected>Admin</option>
                                                    <option value="0" name="Commissioner" selected>Commissioner</option>

                                                </select>

                                            </div>
                                            <div class="form-group mb-5">
                                                <label for="Date">
                                                    Date Registered
                                                </label>
                                                <input type='date' class="form-control" required name="txtDate" />
                                            </div>

                                            <button type="submit" class="btn btn-primary">Edit</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- header -->

                    <div class="mt-5 ">

                        <?php
                                        require('connect-DB.php');

                                        //truy vấn
                                        $sql = "SELECT userName, userEmail, userLevel, idUser FROM user";
                                        $result = mysqli_query($connect, $sql);
                                        
                                        //table list user
                                        $i=1;
                                        if(mysqli_query($connect, $sql))
                                        {
                                            echo '<table class="table table-striped pl-4">';
                                            echo '<thead>';
                                            echo '<tr>';
                                            echo '<th scope="col">User Name</th>';
                                            echo '<th scope="col">Email</th>';
                                            echo '<th scope="col">Level</th>';
                                            echo '<th scope="col">Actions</th>';
                                            echo '</tr>';
                                            echo '</thead>';
                                            while($row = mysqli_fetch_assoc($result))
                                            {
                                                echo '<tr>';
                                                echo '<td>'.$row['userName'].'</td>';
                                                echo '<td>'.$row['userEmail'].'</td>';
                                                if($row['userLevel']==1){
                                                    echo '<td> Admin</td>';
                                                }
                                                else {
                                                    echo'<td> Commissionerin </td>';
                                                }
                                            
                                                echo '<td> <button class="btn btn-sm btn-outline-success  " data-toggle="modal" data-target="#editUser">Edit</button>
                                                        <a onclick="myFunction()" class="btn btn-sm btn-outline-danger " href="delete-user.php?id='.$row['idUser'].'">Delete</a>';
                                                echo'</td>';
                                                echo '</tr>';
                                                $i++;
                                            }
                                            echo '</table>'; 
                                        }
                                        else {
                                            echo 'thôi, lỗi rồi!!!';
                                        }
                    
                                                        ?>


                    </div>

               
            </div>



        </div>
        <!-- JAVASCRIPT
            ================================================== -->
        <?php
        include("partials/scripts.html");
        ?>
        <script>
        function myFunction() {

            if (!confirm("Delete this?")) {
                $('a').attr("href", "view-user.php")
            }
        }
        </script>
</body>

</html>