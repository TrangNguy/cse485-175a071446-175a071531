<?php
session_start();

if (!isset( $_SESSION['id']) )
{ 
   header("Location: LoginAdmin1.php");
   exit();
   
}
 else{
    if(isset( $_SESSION['id'])){
        if($_SESSION['user_level']== 1){
            header('Location:PageAdmin.php');
        }
     
    }

 }


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Header
        ================================================== -->
    <?php
        include("partials/head.html")
        ?>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-icons/3.0.1/iconfont/material-icons.min.css" />
</head>

<body>
    <div class=" color-light p-0">
          <!-- nav-header -->
            <nav class="navbar navbar-expand-md navbar-admin navbar-light d-flex justify-content-between ">
                <div class="brand my-4">
                    <div class="logo">
                        <span class="l l1"></span>
                        <span class="l l2"></span>
                        <span class="l l3"></span>
                        <span class="l l4"></span>
                        <span class="l l5"></span>
                    </div> NTT Admin
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebar-collapse"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <a class="btn btn-outline-success " href="process-logout.php">Log out</a>
            </nav>
        <div class="admin p-0">
          
            <div class="row no-gutters">
                <div class="menu-admin col-sm-3 col-lg-3 navbar-collapse " id="sidebar-collapse">
                    <ul class="navbar-nav ">
                        <li class="nav-item active"><a href="PageAdmin.php"
                                class="nav-link  px-2 d-flex align-items-center "><i class=" mr-2 material-icons icon">
                                    dashboard
                                </i>Dashboard</a></li>
                        <li class="nav-item"><a href="post.php" class="nav-link px-2 d-flex align-items-center"><i
                                    class=" mr-2 material-icons icon">
                                    settings
                                </i>New post</a></li>
                        <li class="nav-item"><a href="list-category.php"
                                class="nav-link sideMenuToggler px-2 d-flex align-items-center"><i
                                    class="mr-2 material-icons icon">view_list
                                </i>List category</a></li>
                    </ul>
                </div>

                <div class="col-sm-9  col-lg-9 content   ">

                    <!-- list bài viết  -->
                    <div class="mt-5 ml-4">
                        <h2 class="text-uppercase">Overview</h2>
                       
                        <div class="row">
                        
                        <?php
                        require('connect-DB.php');
                        $sql = "SELECT COUNT(*) FROM posts";
                        $sql_cate ="SELECT COUNT(*) FROM category";
                        $result = mysqli_query($connect, $sql);
                        $result_cate = mysqli_query($connect, $sql_cate );

                        ?>
                           <div class="col-md-4 ">
                              <div class="card card-dashboard">
                                <div class="card-body text-center">
                                   <span class="text-uppercase">post</span>
                                   <p class="h1"><?php if($result){
                                       $row=mysqli_fetch_assoc($result);
                                       echo $row['COUNT(*)'];
                                   }?></p>
                                </div>
                              </div>
                           </div
                           >
                           <div class="col-md-4 ">
                           <div class="card card-dashboard">
                                <div class="card-body text-center">
                                   <span class="text-uppercase">Category</span>
                                   <p class="h1"><?php if($result_cate){
                                       $row=mysqli_fetch_assoc($result_cate);
                                       echo $row['COUNT(*)'];
                                   }?></p>
                                </div>
                              </div>
                           </div>
                           <div class="col-md-4 col-sm-6">
                           </div>

                        </div>
                    </div>
                
                    
                </div> <!-- content  -->
            </div> <!-- row -->

        </div> <!-- container -->

        <!-- JAVASCRIPT
            ================================================== -->
        <?php
        include("partials/scripts.html")
        ?>

</body>

</html>