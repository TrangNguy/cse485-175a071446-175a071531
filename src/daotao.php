<html>

<head>
<?php
        include("partials/head.html")
        ?>
</head>

<body>
    <!-- Header
        ================================================== -->
        <?php
        include("partials/header.html")
        ?>

    <!-- Main
        ================================================== -->
    <main role="main">
        <section>
            <div class="container">
                <div class="row">
                    <div class="sidebar col-md-3 mt-4 p-0 ">
                        <div class="title p-4">
                            <p class="h6">ĐẠI HỌC NGUYỄN TẤT THÀNH</p>
                            <p class="para-first font-weight-bold">"Đoàn kết-Hội nhập-Năng Động-Trí tuệ-Trách nhiệm</p>
                        </div>
                        <div class="description p-4">
                            <img src="assets/imgage/article_sepecial/academics_thumb.png" alt=""
                                class="img-fluid pb-3">
                            <p>QUY MÔ ĐÀO TẠO</p>
                            <ul class="font-weight-bold">
                                <li>Hơn 20.000 sinh viên</li>
                            </ul>
                            <p>ĐỘI NGŨ GIẢNG VIÊN</p>
                            <ul class="font-weight-bold">
                                <li>924 giảng viên</li>
                                <li>90% có bằng TS, ThS </li>
                            </ul>
                            <p>CƠ SỞ VẬT CHẤT</p>
                            <ul class="font-weight-bold">
                                <li>8 cơ sở đào tạo </li>
                                <li>100.000 m² sàn xây dựng</li>
                                <li>3.000 máy tính</li>
                                <li>100.000 bản sách trong nước và quốc tế</li>
                                <li>Thư viện đạt chuẩn Quốc gia</li>
                            </ul>
                            <p>CHẤT LƯỢNG</p>
                            <ul class="font-weight-bold">
                                <li>Đạt chuẩn kiểm định chất lượng của Bộ GD&ĐT</li>
                                <li>Đạt chuẩn QS-Stars 3 sao (Anh Quốc)</li>
                            </ul>
                            <p>KHOA</p>
                            <ul class="font-weight-bold">
                                <li><a href="#">Y</a></li>
                                <li><a href="#">Dược</a></li>
                                <li><a href="#">Điều dưỡng</a></li>
                                <li><a href="#">Quản trị Kinh doanh</a></li>
                                <li><a href="#">Luật</a></li>
                                <li><a href="#">Tài chính-Kế toàn</a></li>
                                <li><a href="#">Cơ khí-Điện-Điện tử-Ô tô</a></li>
                                <li><a href="#">Kỹ thuật thực phẩm và Môi trường</a></li>
                                <li><a href="#">Công nghệ Sinh học</a></li>
                                <li><a href="#">Công nghệ Thông tin</a></li>
                                <li><a href="#">Âm nhạc</a></li>
                                <li><a href="#">Kiến trúc-Xây dựng-Mỹ thuật ứng dụng</a></li>
                                <li><a href="#">Ngoại ngữ</a></li>
                                <li><a href="#">Du lịch và Việt Nam học</a></li>
                                <li><a href="#">Giáo dục Quốc phòng-An ninh và Giáo dục Thể chất</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <!-- content -->
                        <div class="content pl-3">
                            <img src="assets/imgage/article_sepecial/daotao.png" alt="" class="img-fluid"
                                width="100%">
                            <article class="article_1 p-4 mb-3">
                                <h2 class="h4">Đào tạo</h2>
                                <p>NTTU - Là trường nằm trong doanh nghiệp, ĐH Nguyễn Tất Thành hiểu được quy luật tất
                                    yếu của công tác đào tạo là phải đảm bảo được yêu cầu của doanh nghiệp và đáp ứng
                                    được nhu cầu của xã hội, trong đó chuẩn đầu ra được xem là quy luật trọng yếu. Theo
                                    đó, sinh viên ra trường phải đạt được tiêu chuẩn cần thiết về kiến thức, kỹ năng và
                                    thái độ. Tùy vào mỗi ngành học, sinh viên sẽ có những yêu cầu về chuẩn ngoại ngữ,
                                    tin học, chuẩn kỹ năng và bổ sung nhiều kiến thức khác nhau.</p>
                                <a href="#" class="font-weight-bold ">
                                    <i class="fa fa-chevron-circle-right px-2   "></i>http://phongdaotao.ntt.edu.vn</a>
                            </article>

                            <!-- bai viet -->
                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/daotao_1.jpg" alt=""
                                                    class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Khối Khoa học sức khỏe</a> </h5>
                                                NTTU - Là trọng tâm của Chiến lược phát triển ĐH Nguyễn Tất Thành, khối
                                                khoa học sức khỏe gồm khoa Y, khoa Dược và khoa Điều dưỡng, đào tạo các
                                                ngành khoa học và chăm sóc sức khỏe bao gồm Dược học, Điều dưỡng, Kỹ
                                                thuật y học v.v. Thông qua với việc hợp tác với gần 100 bệnh viện và
                                                công ty dược hàng đầu trong cả nước, sinh viên Trường có cơ hội thực tập
                                                trong môi trường thực tế ngay trong quá trình đào tạo.
                                                <ul class="list-unstyled pt-2">
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Khoa Dược</a>
                                                    </li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Khoa Y</a>
                                                    </li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Khoa Yg</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> <!-- row -->
                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body my-3 pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/Admission_Procedure.jpg"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Khối Kinh tế - Quản trị</a></h5>
                                                NTTU - Khối Kinh tế - Quản trị bao gồm khoa Quản trị kinh doanh, Luật kinh tế và Tài chính – Kế toán. Tận dụng thế mạnh là trường trong doanh nghiệp, là thành viên của Tập đoàn Dệt May Việt Nam, thành viên khối viện – trường Bộ Công thương, việc đào tạo tại các khoa trong khối kinh tế - quản trị luôn gắn liền với đơn vị sử dụng lao động, giúp sinh viên sớm có kinh nghiệm thực tiễn ngay trong quá trình học.
                                                <ul class="list-unstyled pt-2">
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i> Khoa Tài chính Kế toán</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i>Khoa Quản trị kinh doanh</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i> Khoa Luật</a>
                                                        </li>
                                                    </ul>
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body my-3 pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/kythuat_congnghe.png"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Khối Kỹ thuật - Công nghệ</a></h5>
                                                NTTU - Với 4 khoa, Khối kỹ thuật – công nghệ không chỉ lớn về số lượng sinh viên mà còn mạnh về chất lượng đào tạo. Sinh viên tốt nghiệp kỹ thuật – công nghệ của Trường đều có việc làm trong vòng 6 tháng sau khi tốt nghiệp. Giảng viên các khoa kỹ thuật – công nghiệp hiện đang chủ nhiệm nhiều đề tài cấp nhà nước, cấp bộ về công nghệ thông tin, cơ khí, điện, điện tự, nông nghiệp, hóa.
                                                <ul class="list-unstyled pt-2">
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Khoa công nghệ thông tin</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Khoa Kỹ thuật thực phẩm và Môi trường</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Khoa Cơ khí - Điện - Điện tử - Ô tô</a>
                                                        </li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Khoa Công nghệ sinh học</a>
                                                        </li>
                                                    </ul>
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body my-3 pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/khxh-nv-thumb.png" alt=""
                                                    class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#">Khối Khoa học xã hội - Nhân văn</a> </h5>
                                                NTTU - Khối khoa học xã hội – nhân văn phụ trách đào tạo các ngành ngoại ngữ (tiếng Anh, tiếng Trung, tiếng Nhật, tiếng Hàn), ngành Việt Nam học, các ngành Quản trị nhà hàng, khách sạn, du lịch lữ hành. Việc đào tạo các ngành trong khối khoa học xã hội – nhân văn đều theo định hướng ứng dụng, trên cơ sở chuẩn năng lực mà người học cần có để làm việc hiệu quả trong công việc tương lai.
                                                <ul class="list-unstyled pt-2">
                                                    <li> <a href="#"> <i class="fa fa-chevron-right"></i></i>Khoa Ngoại ngữ</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i> Khoa Du lịch và Việt Nam học</a> </li>
                                                </ul>
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>
                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body my-3 pl-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/mythuat-nghethuat.jpg"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">
                                                <h5><a href="#"> Khối Mỹ thuật - Nghệ thuật</a></h5>
                                                NTTU - Khối Mỹ thuật – Nghệ thuật gồm 2 khoa: Khoa Kiến trúc – Xây dựng – Mỹ thuật ứng dụng và Khoa Âm nhạc. Trong đó, Khoa Kiến trúc – Xây dựng – Mỹ thuật ứng dụng là sự kết hợp giữa mỹ thuật, ứng dụng và kỹ thuật nhằm đào tạo đội ngũ kiến trúc sư, chuyên viên thiết kế nội thất, kỹ sư xây dựng phục vụ các công trình xây dựng. Ngoài ra, các khối Mỹ thuật – Nghệ thuật còn đào tạo các ngành thanh nhạc, đạo diễn, diễn viên v.v.
                                                <ul class="list-unstyled pt-2">
                                                    <li> <a href="#"> <i class="fa fa-chevron-right"></i></i> Khoa Kiến trúc - Xây dựng - Mỹ thuật ứng dụng</a></li>
                                                    <li><a href="#"> <i class="fa fa-chevron-right"></i></i>Khoa Âm nhạc</a> </li>
                                                </ul>
                                            </div>
                                            
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>

                            <article>
                                <div class="media py-4 px-0">
                                    <div class="media-body my-3 px-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="assets/imgage/article_sepecial/Trang Dao Tao.jpg"
                                                    alt="" class="img-fulid">
                                            </div>
                                            <div class="col-md-8">

                                                <h5><a href="#">Khối đào tạo quốc tế</a></h5>
                                                Được thành lập vào năm 2008, Viện Đào Tạo Quốc Tế NTT (NIIE) mang đến cho sinh viên một môi trường học tập quốc tế chuyên nghiệp, năng động và sáng tạo với hai hệ chương trình: chương trình liên kết Pearson BTEC HND và chương trình chuẩn quốc tế. NIIE tự hào là học viện đầu tiên tại Việt Nam được công nhận kiểm định bởi tổ chức ASIC (Anh Quốc) và trở thành thành viên chính thức của mạng lưới du học QSIAN. Với tấm bằng tốt nghiệp tại NIIE, sinh viên tự tin việc làm tại các doanh nghiệp trong và ngoài nước, đồng thời dễ dàng tìm kiếm cơ hội học cao hơn tại các trường đại học uy tín thế giới. • Chương trình liên kết • Chương trình chuẩn quốc tế
                                                <ul class="list-unstyled pt-2">
                                                        <li> <a href="#"> <i class="fa fa-chevron-right"></i></i>CHƯƠNG TRÌNH LIÊN KẾT</a></li>
                                                        <li><a href="#"> <i class="fa fa-chevron-right"></i></i>CỬ NHÂN CHUẨN QUỐC TẾ</a> </li>
                                                    </ul>
                                            </div>
                                        </div> <!-- row -->

                                    </div>
                                </div>
                            </article>
                        </div>
                    </div> <!-- col-md-9 -->

                </div>
            </div>
        </section>


    </main>
  <!-- Footer
        ================================================== -->
        <?php
        include("partials/scripts.html")
        ?>

    <!-- JAVASCRIPT
            ================================================== -->
            <?php
        include("partials/scripts.html")
        ?>
</body>

</html>