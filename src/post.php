<?php
session_start();
if (!isset($_SESSION['user_level']))
{ 
   header("Location: LoginAdmin1.php");
   exit();
   
}
echo $_SESSION['name'];
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Header
        ================================================== -->
    <?php
        include("partials/head.html")
        ?>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-icons/3.0.1/iconfont/material-icons.min.css" />
</head>

<body>
    <?php
        include("partials/scripts.html")
        ?>
    <div class="admin p-0">
        <!-- nav-header -->
        <nav class="navbar navbar-expand-md navbar-admin navbar-light d-flex justify-content-between ">
            <div class="brand my-4">
                <div class="logo">
                    <span class="l l1"></span>
                    <span class="l l2"></span>
                    <span class="l l3"></span>
                    <span class="l l4"></span>
                    <span class="l l5"></span>
                </div> NTT Admin
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebar-collapse"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <a class="btn btn-outline-success " href="process-logout.php">Log out</a>
        </nav>
        <div class="row no-gutters">
            <div class="menu-admin col-sm-3 col-lg-3 navbar-collapse " id="sidebar-collapse">
                <ul class="navbar-nav ">
                    <li class="nav-item "><a href="PageAdmin.php"
                            class="nav-link  px-2 d-flex align-items-center "><i class=" mr-2 material-icons icon">
                                dashboard
                            </i>Dashboard</a></li>
                    <li class="nav-item active"><a href="post.php" class="nav-link px-2 d-flex align-items-center"><i
                                class=" mr-2 material-icons icon">
                                settings
                            </i>New post</a></li>
                            <li class="nav-item">
                        <a href="list-category.php"
                                class="nav-link sideMenuToggler px-2 d-flex align-items-center">
                                <i class="mr-2 material-icons icon">view_list
                                </i>List category</a></li>
                </ul>
            </div>

            <div class="col-sm-9  col-lg-9 content   ">

                <div class="header w-100 d-flex justify-content-between mt-3">
                    <h2 class="text-uppercase">List news</h2>
                    <a class="btn btn-outline-success" href="add-post.php">Add Post</a>
                </div>
                <div class="ml-3"> Category:
                    <select name="category">
                        <?php 
                                         require('connect-DB.php');
                                          $sql = "select * from category";
                                          $result = mysqli_query($connect, $sql);
                                          $i = 1;
                                          if($result){
                                    
                                            while($row = mysqli_fetch_assoc($result))
                                               {
                                                   echo '<option value="'.$row['ID'].'">'.$row['Name'].'</option>';
                                                   $i++;
                                               }       
                                          }
                                          else {
                                              echo "loi";
                                          }
                                            
                                     ?>
                    </select>

                </div>
                <!-- list bài viết  -->

                <div class="list-post">

                    <?php
                      require('connect-DB.php');

                      //truy vấn
                      $sql = "SELECT * FROM posts, category WHERE posts.idCat = category.ID";
                      $result = mysqli_query($connect, $sql);
                    
                     
                      //table list 
                      $i = 1;
                      if($result){
                          echo '<div class="table-responsive-lg">';
                        echo '<table class="table table-striped" id ="dtHorizontalExample">';
                        echo '<thead>';
                        echo '<tr>';
                        echo '<th scope="col">#</th>';
                        echo '<th scope="col">Author</th>';
                        echo '<th scope="col">Date Create</th>';
                        echo '<th scope="col">Category</th>';
                       
                        echo '<th scope="col">Title</th>';
                        echo '<th scope="col">Description</th>';  
                        echo '<th scope="col">Image</th>'; 
                        echo '<th scope="col">Actions</th>';                 
                        echo '</tr>';
                        echo '</thead>';
                            while($row = mysqli_fetch_assoc($result)){
                                echo '<tr>';
                                echo '<td>'.$i.'</td>';
                                echo '<td>'.$row['author'].'</td>';
                                echo '<td>'.$row['post-date'].'</td>';
                                echo '<td>'.$row['Name'].'</td>';
                                echo '<td>'.$row['post_title'].'</td>';
                                echo '<td>'.$row['description'].'</td>';
                                echo '<td>  <img src=upload/'.$row['Image'].' " width=200px height = 100px></td>';

                                echo '<td> <button class="btn btn-sm  " data-toggle="modal" data-target= "#edit'.$row['PostId'].'" 
                                id='.$row['PostId'].'><i class="fa fa-edit"  style="font-size: 2em; color: blue;"></i></button>
                              
                                <a onclick="myFunction()" class="btn btn-sm " href="delete-post.php?id='.$row['PostId'].'">
                                <span style="font-size: 2em; color: Tomato;">
                                       <i class="fa fa-trash"></i>
                                 </span></a>';
                        echo'</td>';
                                echo '</tr>';
                                // record
                               
                                ?>
                                
                    <!--modal edit post-->
                    <div class="modal fade " id="edit<?php echo $row['PostId'];?>" tabindex="-1" role="dialog"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit Post</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="edit-post.php" method="POST" enctype="multipart/form-data">
                                <div class="modal-body">
                               
                                    
                                        <div class="card p-5 mb-5">
                                            <div class="form-group row">    
                                                  <input type="hidden" name="id" value=" <?php echo $row['PostId']?>"/>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 font-weight-bold"> Title: </label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" placeholder=""
                                                        name="txtTitle" value="<?php echo $row['post_title']?>"
                                                        required>
                                                 </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 font-weight-bold"> Description: </label>
                                                <div class="col-sm-10">
                                                    <label for="Description">Writing Description</label>
                                                     
                                                    <textarea class="form-control" id="<?php echo "title",$row['PostId']?>" name="txtDes"
                                                        required>   <?php echo $row['description']?></textarea>
                                                        <script>
                                                    CKEDITOR.replace('<?php echo "title",$row['PostId']?>')
                                                    </script>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 font-weight-bold"> Content: </label>
                                                <div class="col-sm-10">
                                                    <label for="Content">Writing Content</label>
                                                   
                                                    <textarea class="form-control" id="<?php echo "content",$row['PostId']?>" rows="3"
                                                        name="txtContent"
                                                        required> <?php echo $row['post_content']?></textarea>
                                                        <script>
                                                        CKEDITOR.replace('<?php echo "content",$row['PostId']?>');
                                                        </script>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-2 font-weight-bold"> Images: </label>
                                                <div class="col-sm-10">
                                                    <div class="images-container">
                                                    <input type="file" name="image" id="image"/>
                                                       <img src="upload/<?php echo $row['Image'] ?>" alt="" width="200px" height ="100px">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 font-weight-bold"> Category: </label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="category">
                                                <?php
                                                 $sql_cate = "select * from category";
                                                 $query = mysqli_query($connect, $sql_cate);
                                                 $catId = $row['idCat'];
                                                 $j = 1;
                                                 if($query){
                                             
                                                     while($row = mysqli_fetch_assoc($query))
                                                     {
                                                         
                                                         ?>
                                                        <option value="<?php echo $row['ID'] ?>"<?php if($catId == $row['ID']) {echo "selected";}?>  ><?php echo $row['Name']?></option>
                                                        <?php $j++;
                                                     }       
                                                 }
                                                 else {
                                                     echo "loi";
                                                 }
                                                ?>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>  <!-- card -->
                                    
                                </div>
                                <div class="modal-footer">
                                   <input type="submit" class="btn btn-primary" name="submit" value="Update"> 
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                </div>
                                </form>
                                <?php
                                            // Xử Lý Upload
                                            var_dump($_FILES);
                                        ?>
                            </div>
                        </div>
                    </div>
                    <!--modal edit post-->
                    <?php
                         $i++;
                            }
                        echo '</table>';
                        echo '</div>';
                      }
                      else {
                          echo "oh oh, loi";
                      }
                ?>


                </div> <!-- list-post -->


            </div>
            <!--content-->
        </div> <!-- row  no-gutters -->

    </div> <!-- container -->

    <!-- JAVASCRIPT
            ================================================== -->

    <script>
    function myFunction() {

        if (!confirm("Delete this?")) {
            $('a').attr("href", "post.php")
        }
    }
   
    </script>
</body>

</html>