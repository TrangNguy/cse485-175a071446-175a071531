<?php
session_start();
if (!isset($_SESSION['user_level']))
{ 
   header("Location: LoginAdmin1.php");
   exit();
   
}


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Header
        ================================================== -->
    <?php
        include("partials/head.html")
        ?>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-icons/3.0.1/iconfont/material-icons.min.css" />
</head>

<body>

    <!-- nav-header -->
    <nav class="navbar navbar-expand-md navbar-admin navbar-light d-flex justify-content-between ">
        <div class="brand my-4">
            <div class="logo">
                <span class="l l1"></span>
                <span class="l l2"></span>
                <span class="l l3"></span>
                <span class="l l4"></span>
                <span class="l l5"></span>
            </div> NTT Admin
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebar-collapse"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <a class="btn btn-outline-success " href="process-logout.php">Log out</a>
    </nav>
    <div class="admin p-0">

        <div class="row no-gutters">
            <div class="menu-admin col-sm-3 col-lg-3 navbar-collapse " id="sidebar-collapse">
                <ul class="navbar-nav ">
                    <li class="nav-item active"><a href="PageAdmin.php"
                            class="nav-link  px-2 d-flex align-items-center "><i class=" mr-2 material-icons icon">
                                dashboard
                            </i>Dashboard</a></li>
                    <li class="nav-item"><a href="post.php" class="nav-link px-2 d-flex align-items-center"><i
                                class=" mr-2 material-icons icon">
                                settings
                            </i>New post</a></li>
                            <li class="nav-item">
                        <a href="list-category.php"
                                class="nav-link sideMenuToggler px-2 d-flex align-items-center">
                                <i class="mr-2 material-icons icon">view_list
                                </i>List category</a></li>
                </ul>
            </div>

            <div class="col-sm-9  col-lg-9 content   ">
                <!-- list-category -->
                
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Category</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                                         require('connect-DB.php');
                                          $sql = "select * from category";
                                          $result = mysqli_query($connect, $sql);
                                          $i = 1;
                                          if($result){
                                    
                                            while($row = mysqli_fetch_assoc($result))
                                               {
                                                   echo "<tr>";
                                                   echo '<td>'.$i.'</td>';
                                                   echo '<td>'.$row['Name'].'</td>';
                                                   echo "</tr>";
                                                   $i++;
                                               }       
                                          }
                                          else {
                                              echo "loi";
                                          }
                                            
                                     ?>
                    </tbody>

                </table>


            </div> <!-- content  -->
        </div> <!-- row -->

    </div> <!-- container -->

    <!-- JAVASCRIPT
            ================================================== -->
    <?php
        include("partials/scripts.html")
        ?>

</body>

</html>